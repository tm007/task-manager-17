package ru.tsc.apozdnov.tm.exeption.field;

public final class DescriptionEmptyExeption extends AbstractFieldExeption {

    public DescriptionEmptyExeption() {
        super("FAULT!! Description is empty!!!");
    }

}
