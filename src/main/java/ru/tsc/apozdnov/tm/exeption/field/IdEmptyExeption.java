package ru.tsc.apozdnov.tm.exeption.field;

public final class IdEmptyExeption extends AbstractFieldExeption {

    public IdEmptyExeption() {
        super("FAULT! Id is empty!!!");
    }

}
