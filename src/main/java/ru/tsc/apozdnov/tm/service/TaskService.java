package ru.tsc.apozdnov.tm.service;

import ru.tsc.apozdnov.tm.api.repository.ITaskRepository;
import ru.tsc.apozdnov.tm.api.service.ITaskService;
import ru.tsc.apozdnov.tm.enumerated.Sort;
import ru.tsc.apozdnov.tm.enumerated.Status;
import ru.tsc.apozdnov.tm.exeption.entity.TaskNotFoundExeption;
import ru.tsc.apozdnov.tm.exeption.field.DescriptionEmptyExeption;
import ru.tsc.apozdnov.tm.exeption.field.IdEmptyExeption;
import ru.tsc.apozdnov.tm.exeption.field.IndexIncorrectExeption;
import ru.tsc.apozdnov.tm.exeption.field.NameEmptyExeption;
import ru.tsc.apozdnov.tm.model.Task;

import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

public class TaskService implements ITaskService {

    private final ITaskRepository taskRepository;

    public TaskService(final ITaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    @Override
    public Task create(final String name) {
        if (name == null || name.isEmpty()) throw new IdEmptyExeption();
        return taskRepository.create(name);
    }

    @Override
    public Task create(final String name, final String description) {
        if (name == null || name.isEmpty()) throw new NameEmptyExeption();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyExeption();
        return taskRepository.create(name, description);
    }

    @Override
    public Task create(final String name, final String description, final Date dateBegin, final Date dateEnd) {
        final Task task = create(name, description);
        if (task == null) throw new TaskNotFoundExeption();
        task.setDateBegin(dateBegin);
        task.setDateEnd(dateEnd);
        return task;
    }

    @Override
    public Task remove(final Task task) {
        if (task == null) throw new TaskNotFoundExeption();
        return taskRepository.remove(task);
    }

    @Override
    public Task add(Task task) {
        if (task == null) throw new TaskNotFoundExeption();
        return taskRepository.add(task);
    }

    @Override
    public List<Task> findAll() {
        return taskRepository.findAll();
    }

    @Override
    public List<Task> findAll(final Comparator comparator) {
        if (comparator == null) return findAll();
        return taskRepository.findAll(comparator);
    }

    @Override
    public List<Task> findAllByProjectId(final String projectId) {
        if (projectId == null || projectId.isEmpty()) return Collections.emptyList();
        return taskRepository.findAllByProjectId(projectId);
    }

    @Override
    public void clear() {
        taskRepository.clear();
    }

    @Override
    public List<Task> findAll(final Sort sort) {
        if (sort == null) return findAll();
        return taskRepository.findAll(sort.getComparator());
    }

    @Override
    public Task findOneById(final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyExeption();
        return taskRepository.findOneById(id);
    }

    @Override
    public Task findOneByIndex(final Integer index) {
        if (index == null || index < 0) throw new IndexIncorrectExeption();
        return taskRepository.findOneByIndex(index);
    }

    @Override
    public Task updateById(final String id, final String name, final String description) {
        if (id == null || id.isEmpty()) throw new IdEmptyExeption();
        if (name == null || name.isEmpty()) throw new NameEmptyExeption();
        final Task task = findOneById(id);
        if (task == null) throw new TaskNotFoundExeption();
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task updateByIndex(final Integer index, final String name, final String description) {
        if (index == null || index < 0) throw new IndexIncorrectExeption();
        final Task task = findOneByIndex(index);
        if (task == null) throw new TaskNotFoundExeption();
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task removeById(final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyExeption();
        return taskRepository.removeById(id);
    }

    @Override
    public Task removeByIndex(final Integer index) {
        if (index == null || index < 0) throw new IndexIncorrectExeption();
        return taskRepository.removeByIndex(index);
    }

    @Override
    public Task changeTaskStatusById(final String id, final Status status) {
        if (id == null || id.isEmpty()) throw new IdEmptyExeption();
        final Task task = findOneById(id);
        if (task == null) throw new TaskNotFoundExeption();
        task.setStatus(status);
        return task;
    }

    @Override
    public Task changeTaskStatusByIndex(final Integer index, final Status status) {
        if (index == null || index < 0) throw new IndexIncorrectExeption();
        final Task task = findOneByIndex(index);
        if (task == null) throw new TaskNotFoundExeption();
        task.setStatus(status);
        return task;
    }

}
