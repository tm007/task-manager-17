package ru.tsc.apozdnov.tm.api.service;

public interface IServiceLocator {

    ILoggerService getloggerService();

    IProjectTaskService getProjectTaskService();

    ITaskService getTaskService();

    IProjectService getProjectService();

    ICommandService getCommandService();

}
