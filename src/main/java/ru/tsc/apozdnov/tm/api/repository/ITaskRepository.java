package ru.tsc.apozdnov.tm.api.repository;

import ru.tsc.apozdnov.tm.model.Task;

import java.util.Comparator;
import java.util.List;

public interface ITaskRepository {

    Task create(String name);

    Task create(String name, String description);

    Task remove(Task task);

    Task add(Task task);

    List<Task> findAll();

    List<Task> findAll(Comparator comparator);

    Task findOneById(String id);

    Task findOneByIndex(Integer index);

    Task removeById(String id);

    Task removeByIndex(Integer index);

    List<Task> findAllByProjectId(String projectId);

    void clear();

}
