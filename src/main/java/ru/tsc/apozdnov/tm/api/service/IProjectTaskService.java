package ru.tsc.apozdnov.tm.api.service;

import ru.tsc.apozdnov.tm.model.Project;

public interface IProjectTaskService {

    boolean bindTaskToProject(String projectId, String taskId);

    boolean unbindTaskFromProject(String projectId, String taskId);

    boolean removeProjectById(String projectId);

    boolean removeProject(Project project);

}
