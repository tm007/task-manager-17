package ru.tsc.apozdnov.tm.component;

import ru.tsc.apozdnov.tm.api.repository.ICommandRepository;
import ru.tsc.apozdnov.tm.api.repository.IProjectRepository;
import ru.tsc.apozdnov.tm.api.repository.ITaskRepository;
import ru.tsc.apozdnov.tm.api.service.*;
import ru.tsc.apozdnov.tm.command.AbstractCommand;
import ru.tsc.apozdnov.tm.command.project.*;
import ru.tsc.apozdnov.tm.command.system.*;
import ru.tsc.apozdnov.tm.command.task.*;
import ru.tsc.apozdnov.tm.enumerated.Status;
import ru.tsc.apozdnov.tm.exeption.system.ArgumentNotSupportedExeption;
import ru.tsc.apozdnov.tm.exeption.system.CommandNotSupportedExeption;
import ru.tsc.apozdnov.tm.model.Project;
import ru.tsc.apozdnov.tm.repository.CommandRepository;
import ru.tsc.apozdnov.tm.repository.ProjectRepository;
import ru.tsc.apozdnov.tm.repository.TaskRepository;
import ru.tsc.apozdnov.tm.service.*;
import ru.tsc.apozdnov.tm.util.DateUtil;
import ru.tsc.apozdnov.tm.util.TerminalUtil;

public final class Bootstrap implements IServiceLocator {

    private final ILoggerService loggerService = new LoggerService();

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final ITaskRepository taskRepository = new TaskRepository();

    private final ITaskService taskService = new TaskService(taskRepository);

    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    {
        registry(new InfoCommand());
        registry(new VersionCommand());
        registry(new HelpCommand());
        registry(new ExitCommand());
        registry(new AboutCommand());
        registry(new ArgumentsCommand());
        registry(new CommandsCommand());
        registry(new CreateTaskCommand());
        registry(new ShowTaskListCommand());
        registry(new ClearTaskListCommand());
        registry(new RemoveTaskByIdCommand());
        registry(new RemoveTaskByIndexCommand());
        registry(new ShowTaskByIndexCommand());
        registry(new ShowTaskByIdCommand());
        registry(new UpdateTaskByIndexCommand());
        registry(new UpdateTaskByIdCommand());
        registry(new ChangeTaskByIdCommand());
        registry(new ChangeTaskByIndexCommand());
        registry(new StartTaskByIdCommand());
        registry(new StartTaskByIndexCommand());
        registry(new CompleteTaskByIdCommand());
        registry(new CompleteTaskByIndexCommand());
        registry(new ShowTaskListByProjectIdCommand());
        registry(new BindTaskToProjectCommand());
        registry(new UnbindTaskFromProjectCommand());
        registry(new CreateProjectCommand());
        registry(new ClearProjectCommand());
        registry(new ShowProjectListCommand());
        registry(new RemoveProjectByIndexCommand());
        registry(new RemoveProjectByIdCommand());
        registry(new UpdateProjectByIdCommand());
        registry(new UpdateProjectByIndexCommand());
        registry(new ShowProjectByIdCommand());
        registry(new ShowProjectByIndexCommand());
        registry(new ChangeProjectByIdCommand());
        registry(new ChangeProjectByIndexCommand());
        registry(new StartProjectByIdCommand());
        registry(new StartProjectByIndexCommand());
        registry(new CompleteProjectByIdCommand());
        registry(new CompleteProjectByIndexCommand());
    }

    private void registry(final AbstractCommand command) {
        command.setServiceLocator(this);
        commandService.add(command);
    }

    private void initDemoData() {
        projectService.add(new Project("silver", "*", Status.NOT_STARTED, DateUtil.toDate("18.08.2021")));
        projectService.add(new Project("berkut", "**", Status.COMPLETED, DateUtil.toDate("13.07.2019")));
        projectService.add(new Project("global", "***", Status.IN_PROGRESS, DateUtil.toDate("07.06.1991")));
        taskService.create("TASK01", "T01");
        taskService.create("TASK02", "T02");
        taskService.create("TASK03", "T03");
    }

    private void iniLogger() {
        loggerService.info("**** Welcome to Task Manager ****");
        Runtime.getRuntime().addShutdownHook(new Thread() {
            public void run() {
                loggerService.info("**** ShitDown Task Manager ****");
            }
        });
    }

    public void run(final String[] args) {
        if (processArgumentTask(args)) System.exit(0);
        initDemoData();
        iniLogger();
        while (true) {
            try {
                System.out.println("Enter command:");
                final String cmd = TerminalUtil.nextLine();
                processCommandTask(cmd);
                System.out.println("****OK****");
                loggerService.command(cmd);
            } catch (Exception ex) {
                loggerService.error(ex);
                System.err.println("***FAULT****");
            }
        }
    }

    public void processCommandTask(final String command) {
        final AbstractCommand abstractCommand = commandService.getCommandByName(command);
        if (abstractCommand == null) throw new CommandNotSupportedExeption(command);
        abstractCommand.execute();
    }

    public void processArgumentTask(final String arg) {
        final AbstractCommand abstractCommand = commandService.getCommandByArgument(arg);
        if (abstractCommand == null) throw new ArgumentNotSupportedExeption(arg);
        abstractCommand.execute();
    }

    public boolean processArgumentTask(final String[] args) {
        if (args == null || args.length == 0) return false;
        final String arg = args[0];
        processArgumentTask(arg);
        return true;
    }

    @Override
    public ILoggerService getloggerService() {
        return loggerService;
    }

    @Override
    public IProjectTaskService getProjectTaskService() {
        return projectTaskService;
    }

    @Override
    public ITaskService getTaskService() {
        return taskService;
    }

    @Override
    public IProjectService getProjectService() {
        return projectService;
    }

    @Override
    public ICommandService getCommandService() {
        return commandService;
    }

}
