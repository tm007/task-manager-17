package ru.tsc.apozdnov.tm.repository;

import ru.tsc.apozdnov.tm.api.repository.IProjectRepository;
import ru.tsc.apozdnov.tm.model.Project;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class ProjectRepository implements IProjectRepository {

    private final List<Project> projects = new ArrayList<>();

    @Override
    public Project create(String name) {
        final Project project = new Project();
        project.setName(name);
        return add(project);
    }

    @Override
    public Project findOneByIndex(Integer index) {
        return projects.get(index);
    }

    @Override
    public Project findById(String id) {
        for (final Project project : projects) {
            if (id.equals(project.getId())) return project;
        }
        return null;
    }

    @Override
    public Project create(String name, String description) {
        final Project project = new Project();
        project.setName(name);
        project.setDescriprion(description);
        return add(project);
    }

    @Override
    public Project remove(Project project) {
        projects.remove(project);
        return project;
    }

    @Override
    public Project removeById(String id) {
        final Project project = findById(id);
        if (project == null) return null;
        projects.remove(project);
        return project;
    }

    @Override
    public Project removeByIndex(Integer index) {
        final Project project = findOneByIndex(index);
        if (project == null) return null;
        projects.remove(project);
        return project;
    }

    @Override
    public boolean existsById(final String id) {
        return findById(id) != null;
    }

    @Override
    public int getSize() {
        return projects.size();
    }

    @Override
    public Project add(final Project project) {
        projects.add(project);
        return project;
    }

    @Override
    public List<Project> findAll() {
        return projects;
    }

    @Override
    public List<Project> findAll(final Comparator comparator) {
        final List<Project> result = new ArrayList<>(projects);
        result.sort(comparator);
        return result;
    }

    @Override
    public void clear() {
        projects.clear();
    }

}
