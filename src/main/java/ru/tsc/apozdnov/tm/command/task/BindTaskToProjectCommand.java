package ru.tsc.apozdnov.tm.command.task;

import ru.tsc.apozdnov.tm.util.TerminalUtil;

public class BindTaskToProjectCommand extends AbstractTaskCommand {

    public static final String NAME = "task-bind-to-project";

    public static final String DESCRIPTION = "Bind task to project.";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("***** BIND TASK TO PROJECT ****");
        System.out.println("ENTER PROJECT ID:");
        final String projectId = TerminalUtil.nextLine();
        System.out.println("ENTER TASK ID:");
        final String taskId = TerminalUtil.nextLine();
        serviceLocator.getProjectTaskService().bindTaskToProject(projectId, taskId);
    }

}
