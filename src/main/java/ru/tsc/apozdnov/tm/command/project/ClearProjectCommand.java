package ru.tsc.apozdnov.tm.command.project;

import ru.tsc.apozdnov.tm.util.TerminalUtil;

import java.util.Date;

public class ClearProjectCommand extends AbstractProjectCommand {

    @Override
    public String getName() {
        return "project-clear";
    }

    @Override
    public String getDescription() {
        return "Clear project.";
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public void execute() {
        System.out.println("**** PROJECT CLEAR ****");
        serviceLocator.getProjectService().clear();
    }

}
