package ru.tsc.apozdnov.tm.command.project;

import ru.tsc.apozdnov.tm.enumerated.Status;
import ru.tsc.apozdnov.tm.model.Project;
import ru.tsc.apozdnov.tm.util.TerminalUtil;

import java.util.Arrays;

public class ChangeProjectByIdCommand extends AbstractProjectCommand {

    @Override
    public String getName() {
        return "project-change-by-id";
    }

    @Override
    public String getDescription() {
        return "Change project by id.";
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public void execute() {
        System.out.println("***** CHANGE PROJECT STATUS BY ID ****");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        System.out.println("ENTER STATUS:");
        System.out.println(Arrays.toString(Status.values()));
        final String statusValue = TerminalUtil.nextLine();
        final Status status = Status.toStatus(statusValue);
        serviceLocator.getProjectService().changeStatusById(id, status);
    }

}
