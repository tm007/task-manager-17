package ru.tsc.apozdnov.tm.command.task;

import ru.tsc.apozdnov.tm.model.Project;
import ru.tsc.apozdnov.tm.util.TerminalUtil;

public class RemoveTaskByIndexCommand extends AbstractTaskCommand {

    public static final String NAME = "task-remove-by-index";

    public static final String DESCRIPTION = "Remove task by index.";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("***** REMOVE PROJECT BY INDEX ****");
        System.out.println("ENTER PROJECT INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        serviceLocator.getTaskService().removeByIndex(index);
        final Project project = serviceLocator.getProjectService().findOneByIndex(index);
        serviceLocator.getProjectTaskService().removeProjectById(project.getId());
    }

}
