package ru.tsc.apozdnov.tm.command.project;

import ru.tsc.apozdnov.tm.enumerated.Status;
import ru.tsc.apozdnov.tm.util.TerminalUtil;

public class CompleteProjectByIdCommand extends AbstractProjectCommand {

    @Override
    public String getName() {
        return "project-complete-by-id";
    }

    @Override
    public String getDescription() {
        return "Complete project by id.";
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public void execute() {
        System.out.println("***** COMPLETE PROJECT BY ID ****");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        serviceLocator.getProjectService().changeStatusById(id, Status.COMPLETED);
    }

}
