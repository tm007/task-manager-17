package ru.tsc.apozdnov.tm.command.project;

import ru.tsc.apozdnov.tm.util.TerminalUtil;

public class UpdateProjectByIndexCommand extends AbstractProjectCommand {

    @Override
    public String getName() {
        return "project-update-by-index";
    }

    @Override
    public String getDescription() {
        return "Update project by index.";
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public void execute() {
        System.out.println("***** UPDATE PROJECT BY INDEX ****");
        System.out.println("ENTER PROJECT INDEX:");
        final Integer id = TerminalUtil.nextNumber() - 1;
        System.out.println("ENTER PROJECT NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER PROJECT DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        serviceLocator.getProjectService().updateByIndex(id, name, description);
    }

}
