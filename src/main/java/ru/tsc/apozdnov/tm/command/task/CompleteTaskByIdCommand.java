package ru.tsc.apozdnov.tm.command.task;

import ru.tsc.apozdnov.tm.enumerated.Status;
import ru.tsc.apozdnov.tm.util.TerminalUtil;

public class CompleteTaskByIdCommand extends AbstractTaskCommand {

    public static final String NAME = "task-complete-by-id";

    public static final String DESCRIPTION = "Complete task by id.";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("***** COMPLETE TASK BY ID ****");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        serviceLocator.getTaskService().changeTaskStatusById(id, Status.COMPLETED);
    }

}
