package ru.tsc.apozdnov.tm.command.task;

import ru.tsc.apozdnov.tm.enumerated.Sort;
import ru.tsc.apozdnov.tm.model.Task;
import ru.tsc.apozdnov.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public class ShowTaskListByProjectIdCommand extends AbstractTaskCommand {

    public static final String NAME = "task-show-by-project-id";

    public static final String DESCRIPTION = "Show task by projectId";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("**** TASK LIST BY PROJECT ID ****");
        System.out.println("ENTER ROJECT ID:");
        final String projectId = TerminalUtil.nextLine();
        final List<Task> tasks = serviceLocator.getTaskService().findAllByProjectId(projectId);
        renderTasks(tasks);
    }

}
