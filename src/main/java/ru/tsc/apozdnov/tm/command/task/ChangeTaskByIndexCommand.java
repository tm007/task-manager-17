package ru.tsc.apozdnov.tm.command.task;

import ru.tsc.apozdnov.tm.enumerated.Status;
import ru.tsc.apozdnov.tm.util.TerminalUtil;

import java.util.Arrays;

public class ChangeTaskByIndexCommand extends AbstractTaskCommand {

    public static final String NAME = "task-change-by-index";

    public static final String DESCRIPTION = "Change task by index.";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("***** CHANGE TASK STATUS BY INDEX ****");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        System.out.println("ENTER STATUS:");
        System.out.println(Arrays.toString(Status.values()));
        final String statusValue = TerminalUtil.nextLine();
        final Status status = Status.toStatus(statusValue);
        serviceLocator.getTaskService().changeTaskStatusByIndex(index, status);
    }

}
